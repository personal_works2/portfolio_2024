import i18n from "i18next";
import Backend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    ebug: true,
    detection: {
      order: ["queryString", "cookie"],
      cache: ["cookie"],
    },
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;

// // Ajoutez ici vos traductions pour chaque langue
// const resources = {
//   en: {
//     translation: {
//       about: "About",
//       professionally: "Professionally",
//       personally: "Personally",
//       interests: "Interests",
//       // ... Ajoutez d'autres traductions selon vos besoins
//     },
//   },
//   fr: {
//     translation: {
//       about: "À propos",
//       professionally: "Professionnellement",
//       personally: "Personnellement",
//       interests: "Centres d'intérêt",
//       // ... Ajoutez d'autres traductions selon vos besoins
//     },
//   },
// };

// i18n.use(initReactI18next).init({
//   resources,
//   lng: "en", // Langue par défaut
//   interpolation: {
//     escapeValue: false, // Not needed for React as it escapes by default
//   },
// });

// export default i18n;
