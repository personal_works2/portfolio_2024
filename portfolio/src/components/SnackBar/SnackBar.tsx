import styled, { keyframes } from "styled-components";
import { Check } from "../../Icon/Icon";
import { COLORS } from "../../utils/palette";
import { useEffect, useState } from "react";

interface SnackBarProps {
  message: string;
}

const hideTimeBarAnimation = keyframes`
  0% { right: 100%; width: 100% }
  100% { right: 0; width: 0}
`;

const showSnackBarAnimation = keyframes`
  from { right: -100%; }
  to { right: 15px; }
`;

const hideSnackBarAnimation = keyframes`
  from { right: 15px; }
  to { right: -100%; }
`;

const StyledSnackBar = styled.div<{ isVisible: boolean }>`
  width: 250px;
  position: fixed;
  display: flex;
  flex-direction: column;
  top: 25px;
  right: 15px;
  border-radius: 4px;
  background-color: var(--night-100);
  color: var(--white-100);
  animation: ${({ isVisible }) =>
      isVisible ? showSnackBarAnimation : hideSnackBarAnimation}
    0.5s forwards;
`;

const Message = styled.h2`
  font-family: sans-serif;
  font-size: 12px;
`;

const TimeBar = styled.div`
  height: 5px;
  width: 100%;
  border-radius: 0 0 4px 4px;
  background-color: var(--grass-100);
  animation: ${hideTimeBarAnimation} 5s forwards;
`;

const Wrapper = styled.div`
  width: 100%;
  height: 30px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

const SnackBar = ({ message }: SnackBarProps): JSX.Element => {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    setIsVisible(true);

    const timer = setTimeout(() => {
      setIsVisible(false);
    }, 5000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <StyledSnackBar isVisible={isVisible}>
      <Wrapper>
        <Check fill={COLORS.GRASS[100]} />
        <Message>{message}</Message>
      </Wrapper>
      <TimeBar />
    </StyledSnackBar>
  );
};

export default SnackBar;
