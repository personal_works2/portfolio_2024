import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import styled from "styled-components";

interface CarouselProps {
  images: [];
}

const CarouselContainer = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

const Carousel = ({ images }: CarouselProps): JSX.Element => {
  const [positionIndexes, setPositionIndexes] = useState([0, 1, 2, 3, 4]);

  const positions = ["center", "left1", "left", "right", "right1"];

  const imageVariants = {
    center: { x: "0%", scale: 1, zIndex: 5 },
    left1: { x: "-50%", scale: 0.7, zIndex: 3 },
    left: { x: "-90%", scale: 0.5, zIndex: 2 },
    right: { x: "90%", scale: 0.5, zIndex: 1 },
    right1: { x: "50%", scale: 0.7, zIndex: 3 },
  };

  const transition = { duration: 0.5, ease: "easeInOut" };
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  const handleImageClick = (index: number) => {
    setPositionIndexes((prevIndexes = []) => {
      const updatedIndexes = [...prevIndexes];
      updatedIndexes.splice(index, 1);
      updatedIndexes.unshift(index);
      return updatedIndexes;
    });
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setPositionIndexes((prevIndexes = []) => {
        const updatedIndexes = [...prevIndexes];
        updatedIndexes.push(updatedIndexes.shift() || 0);
        return updatedIndexes;
      });
    }, 3000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <CarouselContainer>
      {images.map((image: any, index: any) => (
        <motion.img
          key={index}
          src={image}
          alt={image}
          className="rounded-[12px]"
          initial="center"
          animate={positions[positionIndexes[index]]}
          variants={imageVariants}
          transition={transition}
          style={{
            width: windowWidth < 768 ? "40%" : "30%",
            position: "absolute",
          }}
          onClick={() => handleImageClick(index)}
        />
      ))}
    </CarouselContainer>
  );
};

export default Carousel;
