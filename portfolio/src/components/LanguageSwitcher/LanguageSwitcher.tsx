import styled from "styled-components";
import { useTranslation } from "react-i18next";

const LanguageWrapper = styled.div`
  display: flex;
  font-size: 30px;
`;

const LanguageButton = styled.button`
  font-size: 20px;
  border: none;
  background-color: transparent;
  color: var(--gradient-light-grey);
  cursor: pointer;

  &:hover {
    color: black;
    transform: scale(1.2);
  }
`;

const LanguageSwitcher = () => {
  const { i18n } = useTranslation();

  const handleChangeLanguage = (language: any) => {
    i18n.changeLanguage(language);
    localStorage.setItem("selectedLanguage", language);
  };

  return (
    <LanguageWrapper id="top">
      <LanguageButton onClick={() => handleChangeLanguage("en")}>
        EN
      </LanguageButton>
      |
      <LanguageButton onClick={() => handleChangeLanguage("fr")}>
        FR
      </LanguageButton>
    </LanguageWrapper>
  );
};

export default LanguageSwitcher;
