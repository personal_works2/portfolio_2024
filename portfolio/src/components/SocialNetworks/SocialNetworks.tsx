import styled from "styled-components";
import gitlab from "../Works/assets/gitlab.png";
import linkedin from "../Works/assets/linkedin.png";

const StyledSocialNetworks = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: fixed;
  gap: 15px;
  right: 5%;
  top: 5%;
`;

const Link = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 30px;
  cursor: pointer;
  filter: grayscale(100%);

  &:hover {
    filter: grayscale(0%);
  }
`;

const SocialNetworks = () => {
  return (
    <StyledSocialNetworks>
      <Link>
        <img src={gitlab} alt="gitlab-network" />
      </Link>
      <Link>
        <img src={linkedin} alt="linkedin-network" style={{ width: "25px" }} />
      </Link>
    </StyledSocialNetworks>
  );
};

export default SocialNetworks;
