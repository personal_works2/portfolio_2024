import styled, { keyframes } from "styled-components";

const slideFromTop = keyframes`
  from {
    top: -100%;
    opacity: 0;
  }
  to {
    top: 0;
    opacity: 1;
  }
`;

const StyledIntro = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  animation: ${slideFromTop} 2.5s forwards;

  h1 {
    font-size: 100px;
  }
`;

const Intro = () => {
  return (
    <StyledIntro>
      <h1>Hello</h1>
    </StyledIntro>
  );
};

export default Intro;
