import React, { ChangeEvent, useRef, useState } from "react";
import emailjs from "@emailjs/browser";
import styled, { css } from "styled-components";
import SnackBar from "../SnackBar/SnackBar";
import TitleContainer from "../TitleContainer/TitleContainer";
import { Logo } from "../../Icon/Icon";
import { COLORS } from "../../utils/palette";

const StyledContactForm = styled.div`
  width: 50%;
  display: flex;
  justify-content: center;
  align-items: start;
  flex-direction: column;
  text-align: start;
  margin-top: 25%;
  gap: 20px;
`;

const ContactParagraph = styled.p`
  font-family: sans-serif;
  font-variant: small-caps;
`;

const Form = styled.form`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  width: 100%;
  margin-bottom: 100px;
  font-size: 16px;
`;

const Input = styled.input`
  width: 100%;
  height: 35px;
  padding: 7px;
  outline: none;
  border-radius: 5px;
  border: 1px solid var(--night-100);

  &:focus {
    border: 2px solid var(--night-100);
  }
`;

const Label = styled.label`
  margin-top: 1rem;
`;

const Textarea = styled.textarea`
  max-width: 100%;
  min-width: 100%;
  width: 100%;
  max-height: 100%;
  min-height: 50%;
  padding: 7px;
  outline: none;
  border-radius: 5px;
  border: 1px solid var(--night-100);

  &:focus {
    border: 2px solid var(--night-100);
  }
`;

const SubmitButton = styled.button<{ hasErrors: boolean }>`
  width: 100%;
  text-align: center;
  font-size: 16px;
  margin-top: 2rem;
  cursor: pointer;
  color: white;
  border: none;
  border-radius: 5px;
  background: var(--sky-100);

  ${({ disabled }) =>
    disabled &&
    css`
      background: var(--disabled-100);
      cursor: not-allowed;
    `}
`;

const ErrorMessage = styled.div`
  color: var(--gaspacho-100);
`;

const LogoContainer = styled.div`
  width: 100%;
  margin-bottom: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormContact = (): JSX.Element => {
  const [isMessageSent, setIsMessageSent] = useState(false);
  const [hasFormErrors, setHasFormErrors] = useState(false);
  const form = useRef<HTMLFormElement>(null);

  const [formData, setFormData] = useState({
    user_name: "",
    user_email: "",
    message: "",
  });
  const [formErrors, setFormErrors] = useState({
    user_name: "",
    user_email: "",
    message: "",
  });

  const validateEmail = (email: string) => {
    return /\S+@\S+\.\S+/.test(email);
  };

  const handleChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    let errors = { ...formErrors };

    if (!formData.user_name) {
      errors.user_name = "Veuillez entrer votre nom";
    }

    if (!formData.user_email) {
      errors.user_email = "Veuillez entrer votre adresse e-mail";
    } else if (!validateEmail(formData.user_email)) {
      errors.user_email = "Adresse e-mail invalide";
    }

    if (!formData.message) {
      errors.message = "Veuillez entrer votre message";
    }

    if (Object.values(errors).every((error) => error === "")) {
      emailjs
        .sendForm(
          "service_1acce2u",
          "template_0t4weft",
          form.current as HTMLFormElement,
          {
            publicKey: "bG7uvHiAxi8i02xlZ",
          }
        )
        .then(
          () => {
            console.log("SUCCESS!");
            setIsMessageSent(true);
            setFormData({
              user_name: "",
              user_email: "",
              message: "",
            });
            setFormErrors({
              user_name: "",
              user_email: "",
              message: "",
            });
          },
          (error) => {
            console.log("FAILED...", error.text);
          }
        );
    } else {
      setFormErrors(errors);
      setHasFormErrors(true);
    }
  };

  return (
    <StyledContactForm id="contact">
      <TitleContainer title={"Contact"} />
      <ContactParagraph>
        Lorem ipsum dolor sit amet consectetur. Mi in tellus nibh diam. Tempor
        orci ultricies risus tristique condimentum eget blandit nisl enim. In
        tincidunt habitant nec dictum augue euismod volutpat faucibus est.
        Viverra bibendum ipsum a ac rutrum at.
      </ContactParagraph>
      {isMessageSent && <SnackBar message="Message envoyé avec succès !" />}

      <Form ref={form} onSubmit={handleSubmit}>
        <Label htmlFor="user_name">Name</Label>
        <Input
          type="text"
          id="user_name"
          name="user_name"
          value={formData.user_name}
          onChange={handleChange}
        />
        {formErrors.user_name && (
          <ErrorMessage>{formErrors.user_name}</ErrorMessage>
        )}

        <Label htmlFor="user_email">Email</Label>
        <Input
          type="email"
          id="user_email"
          name="user_email"
          value={formData.user_email}
          onChange={handleChange}
        />
        {formErrors.user_email && (
          <ErrorMessage>{formErrors.user_email}</ErrorMessage>
        )}

        <Label htmlFor="message">Message</Label>
        <Textarea
          id="message"
          name="message"
          value={formData.message}
          onChange={handleChange}
        />
        {formErrors.message && (
          <ErrorMessage>{formErrors.message}</ErrorMessage>
        )}

        <SubmitButton
          type="submit"
          disabled={
            !formData.user_name || !formData.user_email || !formData.message
          }
          hasErrors={hasFormErrors}
        >
          Send
        </SubmitButton>
      </Form>
      <LogoContainer>
        <Logo fill={COLORS.NIGHT[100]} width={30} />
      </LogoContainer>
    </StyledContactForm>
  );
};

export default FormContact;
