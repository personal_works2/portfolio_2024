import styled from "styled-components";

interface TitleContainerProps {
  title: string;
}

const StyledTitle = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 36px;
  width: 100%;
`;

const Title = styled.h2`
  font-size: 50px;
`;

const Divider = styled.div`
  width: 70%;
  background-color: var(--sky-100);
  height: 1px;
  z-index: 1;
`;

const TitleContainer = ({ title }: TitleContainerProps): JSX.Element => {
  return (
    <StyledTitle>
      <Title>{title}</Title>
      <Divider />
    </StyledTitle>
  );
};

export default TitleContainer;
