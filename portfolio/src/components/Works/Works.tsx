import styled, { keyframes } from "styled-components";
import gitlab from "./assets/gitlab.png";
import react2 from "./assets/react2.png";
import reactnative2 from "./assets/reactnative2.webp";
import typescript from "./assets/typescript.png";
import javascript from "./assets/javascript.png";
import ParagraphContainer from "../ParagraphContainer/ParagraphContainer";
import PicturesMosaic from "../MosaicPictures/MosaicPictures";
import TitleContainer from "../TitleContainer/TitleContainer";
import { useTranslation } from "react-i18next";

const fromNormalToBigger = keyframes`
  from {
    transform: scale(1);
  }
  to {
    transform: scale(1.2);
  }
`;

const fromBiggerToNormal = keyframes`
  0% {
    transform: scale(1.2);
  }
  100% {
    transform: scale(1);
  }
`;

const StyledWorks = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  text-align: start;
  margin-top: 10%;
  gap: 24px;
`;

const SubtitleWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  justify-content: center;
  align-items: center;
  gap: 24px;
  margin-top: 100px;
`;

const MosaicWrapper = styled.div`
  display: flex;
  gap: 36px;
  font-family: sans-serif;
  font-variant: small-caps;
  align-items: center;
`;

const Divider = styled.div`
  width: 100%;
  background-color: var(--sky-100);
  height: 1px;
`;

const ImagesWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-family: sans-serif;
  font-variant: small-caps;

  @media (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

const ImagesColumn = styled.div`
  display: flex;
  flex-direction: column;
  text-align: left;
`;

const ImageContainer = styled.div`
  background-color: white;
  box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 120px;
  height: 120px;
`;

const Image = styled.img`
  width: 80px;
  height: 80px;
  filter: grayscale(100%);
  transition: filter 0.5s ease;

  &:hover {
    filter: grayscale(0%);
    animation: ${fromNormalToBigger} 0.5s ease-in-out forwards;
  }

  &:not(:hover) {
    animation: ${fromBiggerToNormal} 0.5s ease-in-out forwards;
  }
`;

const Works = () => {
  const { t } = useTranslation();

  const imagesWithDescription = [
    { src: react2, name: "React" },
    { src: reactnative2, name: "React Native" },
    { src: javascript, name: "Javascript" },
    { src: typescript, name: "TypeScript" },
    { src: gitlab, name: "GitLab" },
  ];

  const images = [
    react2,
    reactnative2,
    javascript,
    typescript,
    gitlab,
    javascript,
    react2,
    reactnative2,
    javascript,
  ];

  return (
    <StyledWorks id="works">
      <TitleContainer title={t("works")} />
      <h2>Alvie</h2>
      <ParagraphContainer
        title={"Context"}
        message={
          "Lorem ipsum dolor sit amet consectetur. Mi in tellus nibh diam.Tempor orci ultricies risus tristique condimentum eget blandit nislenim. In tincidunt habitant nec dictum augue euismod volutpat faucibus est. Viverra bibendum ipsum a ac rutrum at."
        }
      />

      <MosaicWrapper>
        <p>
          Lorem ipsum dolor sit amet consectetur. Mi in tellus nibh diam.Tempor
          orci ultricies risus tristique condimentum eget blandit nislenim. In
          tincidunt habitant nec dictum augue euismod volutpat faucibus est.
          Viverra bibendum ipsum a ac rutrum at.
        </p>
        <PicturesMosaic images={images} />
      </MosaicWrapper>

      <MosaicWrapper>
        <PicturesMosaic images={images} />
        <p>
          Lorem ipsum dolor sit amet consectetur. Mi in tellus nibh diam.Tempor
          orci ultricies risus tristique condimentum eget blandit nislenim. In
          tincidunt habitant nec dictum augue euismod volutpat faucibus est.
          Viverra bibendum ipsum a ac rutrum at.
        </p>
      </MosaicWrapper>

      <SubtitleWrapper>
        <h2>Technos</h2>
        <Divider />
      </SubtitleWrapper>
      <ImagesWrapper>
        {imagesWithDescription.map((image: any, index: any) => (
          <ImagesColumn>
            <ImageContainer>
              <Image key={index} src={image.src} alt={image.name} />
            </ImageContainer>
            <p>{image.name}</p>
          </ImagesColumn>
        ))}
      </ImagesWrapper>
    </StyledWorks>
  );
};

export default Works;
