import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import styled, { keyframes } from "styled-components";
import Carousel from "../Carousel/Carousel";
import ParagraphContainer from "../ParagraphContainer/ParagraphContainer";
import TitleContainer from "../TitleContainer/TitleContainer";
import studio1 from "./assets/studio1.jpg";
import studio2 from "./assets/studio2.jpeg";
import concert from "./assets/concert.jpeg";
import history from "./assets/history.png";
import chess from "./assets/chess.png";
import guitar from "./assets/guitar.png";

const fromNormalToBigger = keyframes`
  from {
    transform: scale(1);
  }
  to {
    transform: scale(1.2);
  }
`;

const AboutContainer = styled.div`
  display: flex;
  width: 100%;
  margin-top: 5%;
  text-align: left;
  flex-direction: column;
  gap: 100px;
`;

const AboutWrapper = styled.div`
  display: flex;
  width: 100%;
  text-align: left;
  flex-direction: column;
  justify-content: space-around;
  gap: 100px;
`;

const fromBiggerToNormal = keyframes`
  0% {
    transform: scale(1.2);
  }
  100% {
    transform: scale(1);
  }
`;

const AboutPictures = styled.div`
  opacity: 0;
  transform: translateX(-300px);
  transition: transform 1s ease-out, opacity 1s;

  &.active {
    transform: translateY(0);
    opacity: 1;
  }
`;

const PersonallyH3 = styled.h3`
  font-family: sans-serif;
  font-variant: small-caps;
  color: var(--sky-100);
  font-size: 24px;
`;

const Interests = styled.div`
  display: flex;
  flex-direction: column;
`;

const Subtitle = styled.div`
  display: flex;
  align-items: center;
  font-family: sans-serif;
  justify-content: center;
  font-size: 25px;
  gap: 36px;
  color: var(--lake-200);
`;

const InterestsDivider = styled.div`
  width: 100%;
  background-color: var(--sky-100);
  height: 1px;
`;

const ImagesWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-family: sans-serif;
  font-variant: small-caps;

  @media (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

const ImagesColumn = styled.div`
  display: flex;
  flex-direction: column;
  text-align: left;
`;

const Image = styled.img`
  max-width: 200px;
  max-height: 200px;
  filter: grayscale(100%);
  transition: filter 0.5s ease;

  &:hover {
    filter: grayscale(0%);
    animation: ${fromNormalToBigger} 0.5s ease-in-out forwards;
  }

  &:not(:hover) {
    animation: ${fromBiggerToNormal} 0.5s ease-in-out forwards;
  }
`;

const About = () => {
  const { t, i18n } = useTranslation();

  const [isActive, setIsActive] = useState(false);

  const aboutPicturesRef = useRef<HTMLDivElement>(null);
  const personallyRef = useRef<HTMLDivElement>(null);
  const professionnallyRef = useRef<HTMLDivElement>(null);
  const aboutPersonnallyPicturesRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const aboutPicturesCurrentRef = aboutPicturesRef.current;
    const personallyCurrentRef = personallyRef.current;
    const professionnallyCurrentRef = professionnallyRef.current;
    const aboutPersonnallyPicturesCurrentRef =
      aboutPersonnallyPicturesRef.current;

    const observerAboutPictures = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting) {
        aboutPicturesCurrentRef?.classList.add("active");
      }
    });

    const observerPersonally = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting) {
        personallyCurrentRef?.classList.add("active");
      }
    });

    const observerProfessionnally = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting) {
        setIsActive(true);
      }
    });

    const observerProfessionnallyPictures = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          aboutPersonnallyPicturesCurrentRef?.classList.add("active");
        }
      }
    );

    if (aboutPicturesCurrentRef) {
      observerAboutPictures.observe(aboutPicturesCurrentRef);
    }

    if (personallyCurrentRef) {
      observerPersonally.observe(personallyCurrentRef);
    }

    if (professionnallyCurrentRef) {
      observerProfessionnally.observe(professionnallyCurrentRef);
    }

    if (aboutPersonnallyPicturesCurrentRef) {
      observerProfessionnallyPictures.observe(
        aboutPersonnallyPicturesCurrentRef
      );
    }

    const storedLanguage = localStorage.getItem("selectedLanguage");
    if (storedLanguage) {
      i18n.changeLanguage(storedLanguage);
    }

    return () => {
      if (aboutPicturesCurrentRef) {
        observerAboutPictures.unobserve(aboutPicturesCurrentRef);
      }

      if (personallyCurrentRef) {
        observerPersonally.unobserve(personallyCurrentRef);
      }

      if (professionnallyCurrentRef) {
        observerProfessionnally.unobserve(professionnallyCurrentRef);
      }

      if (aboutPersonnallyPicturesCurrentRef) {
        observerProfessionnally.unobserve(aboutPersonnallyPicturesCurrentRef);
      }
    };
  }, [i18n]);

  const professtionalsImages: any = [
    concert,
    studio1,
    studio2,
    concert,
    studio1,
  ];

  const imagesWithDescription = [
    { src: chess, name: t("interests.game") },
    { src: history, name: t("interests.history") },
    { src: guitar, name: t("interests.guitar") },
  ];
  return (
    <AboutContainer id="about">
      <TitleContainer title={t("about")} />
      <AboutWrapper>
        <AboutPictures ref={aboutPicturesRef}>
          <Carousel images={professtionalsImages} />
        </AboutPictures>
        <ParagraphContainer
          ref={professionnallyRef}
          // isActive={isActive}
          title={t("professionnal.professionally")}
          message={t("professionnal.description")}
        />
        <AboutPictures ref={aboutPersonnallyPicturesRef}>
          <Carousel images={professtionalsImages} />
        </AboutPictures>
        <ParagraphContainer
          reverse={true}
          ref={professionnallyRef}
          // isActive={isActive}
          title={t("personal.personally")}
          message={t("personal.description")}
        />
      </AboutWrapper>
      <Interests>
        <Subtitle>
          <PersonallyH3>{t("interests.title")}</PersonallyH3>
          <InterestsDivider />
        </Subtitle>
        <ImagesWrapper>
          {imagesWithDescription.map((image: any, index: any) => (
            <ImagesColumn>
              <Image key={index} src={image.src} alt={image.name} />
              <p>{image.name}</p>
            </ImagesColumn>
          ))}
        </ImagesWrapper>
      </Interests>
    </AboutContainer>
  );
};

export default About;
