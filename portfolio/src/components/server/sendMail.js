const nodemailer = require("nodemailer");
require("dotenv").config();

const sendMail = async (name, email, message) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.USER_MAIL_ID,
      pass: process.env.USER_MAIL_PASSWORD,
    },
  });

  const mailOptions = {
    from: `"${name}" <${email}>`,
    to: process.env.USER_MAIL_ID,
    subject: "Prise de contact depuis le Portfolio",
    text: message,
  };

  try {
    const info = await transporter.sendMail(mailOptions);
    console.log("E-mail envoyé avec succès:", info.response);
    return { success: true };
  } catch (error) {
    console.error("Erreur lors de l'envoi de l'e-mail:", error);
    return { success: false, error: "Erreur lors de l'envoi de l'e-mail" };
  }
};

module.exports = sendMail;
