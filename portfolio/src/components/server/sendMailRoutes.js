const express = require("express");
const router = express.Router();
const sendMail = require("./sendMail");

router.post("/api/send-email", async (req, res) => {
  const { name, email, message } = req.body;
  try {
    const result = await sendMail(name, email, message);
    if (result.success) {
      res
        .status(200)
        .json({ success: true, message: "E-mail envoyé avec succès" });
    } else {
      res.status(500).json({ success: false, error: result.error });
    }
  } catch (error) {
    console.error("Erreur lors de l'envoi de l'e-mail:", error);
    res
      .status(500)
      .json({ success: false, error: "Erreur lors de l'envoi de l'e-mail" });
  }
});

module.exports = router;
