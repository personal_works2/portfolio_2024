const express = require("express");
const sendMailRoutes = require("./sendMailRoutes");

const app = express();

app.use(express.json());
app.use("/api", sendMailRoutes);

const PORT = process.env.PORT || 5001;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
