import styled from "styled-components";

const StyledFooter = styled.div`
  width: 100%;
  padding: 8px;
  border-top: 1px solid var(--sky-100);
  font-family: sans-serif;
  display: flex;
  justify-content: space-between;
`;

const Footer = () => {
  const labels = [
    "2024 ©",
    "Robin Godart",
    "All rights reserved",
    "Made with love",
    "Lorem Ipsum",
  ];
  return (
    <StyledFooter>
      {labels.map((label, index) => (
        <p key={index}>{label}</p>
      ))}
    </StyledFooter>
  );
};

export default Footer;
