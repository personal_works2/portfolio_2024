import { LegacyRef } from "react";
import styled from "styled-components";

interface ParagraphContainerProps {
  message: string;
  title: string;
  reverse?: boolean;
  ref?: LegacyRef<HTMLDivElement>;
  isActive?: boolean;
  id?: string;
}

const StyledParagraphContainer = styled.div`
  display: flex;
  align-items: center;
  font-family: sans-serif;
  justify-content: center;
  font-size: 25px;
  gap: 36px;
  margin-bottom: 5%;
  font-family: sans-serif;
  font-variant: small-caps;
`;

const Paragraph = styled.div<{ reverse: boolean }>`
  display: flex;
  flex-direction: ${(props) => (props.reverse ? "row-reverse" : "row")};
  font-family: sans-serif;
  justify-content: center;
  font-size: 22px;
  gap: 36px;
  opacity: 1;
  /* transform: translateY(300px); */
  transition: transform 1s ease-out, opacity 1s;

  /* &.active {
    transform: translateY(0);
    opacity: 1;
  } */
  @media (max-width: 767px) {
    /* display: flex; */
    flex-direction: column;
  }
`;

const Divider = styled.div`
  width: 20%;
  margin-top: 20px;
  background-color: var(--sky-100);
  height: 1px;
`;

const ParagraphH3 = styled.h3`
  color: var(--sky-100);
  font-size: 24px;
`;

const ParagraphP = styled.p`
  font-size: 16px;
`;

const ParagraphContainer = ({
  message,
  title,
  ref,
  reverse,
  // isActive,
  id,
}: ParagraphContainerProps): JSX.Element => {
  return (
    <StyledParagraphContainer
      id={id}
      // className={isActive ? "active" : ""}
      ref={ref}
    >
      <Paragraph reverse={reverse ? true : false}>
        <ParagraphH3>{title}</ParagraphH3>
        <Divider />
        <ParagraphP>{message}</ParagraphP>
      </Paragraph>
    </StyledParagraphContainer>
  );
};

export default ParagraphContainer;
