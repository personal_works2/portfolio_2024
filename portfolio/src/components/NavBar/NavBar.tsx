import styled from "styled-components";
import { Logo } from "../../Icon/Icon";
import { COLORS } from "../../utils/palette";
import { useTranslation } from "react-i18next";

const StyledNavBar = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  border: 1px solid #e2e2e2;
  border-radius: 8px;
  position: fixed;
  bottom: 55px;
  background-color: white;
  box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
  transition: all 0.3s ease;
  padding: 5px;
  z-index: 10;

  div:nth-child(1) {
    margin-left: 40%;
  }
`;

const LogoContainer = styled.div`
  width: 50%;
  margin: 0;
  display: flex;
  justify-content: center;
`;

const NavBarLink = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border-radius: 8px;
`;

const Link = styled.a`
  font-size: 20px;
  color: inherit;
  text-decoration: none;
  position: relative;

  &::before {
    content: "";
    position: absolute;
    left: 10px;
    top: 0;
    height: 100%;
    width: 0;
    background-color: #ffff0092;
    z-index: -1;
    transition: width 0.3s ease;
  }
  &:hover::before {
    width: 110%;
  }
`;

const Spliter = styled.div`
  width: 1px;
  background-color: #e2e2e2;
  height: 40px;
  margin-top: 5px;
  margin-bottom: 5px;
`;

const NavBar = (): JSX.Element => {
  const { t } = useTranslation();
  return (
    <StyledNavBar>
      <a
        style={{ width: "10%" }}
        href="#top"
        onClick={(e) => {
          let el = document.getElementById("top");
          e.preventDefault();
          el && el.scrollIntoView({ behavior: "smooth", block: "start" });
          window.history.pushState("top", "top", "/top");
        }}
      >
        <LogoContainer>
          <Logo width={"100%"} height={"100%"} fill={COLORS.NIGHT[100]} />
        </LogoContainer>
      </a>
      <Spliter />
      <NavBarLink>
        <Link
          href="#about"
          onClick={(e) => {
            let el = document.getElementById("about");
            e.preventDefault();
            el && el.scrollIntoView({ behavior: "smooth", block: "start" });
            window.history.pushState("about", "about", "/about");
          }}
        >
          {t("about")}
        </Link>
      </NavBarLink>
      <NavBarLink>
        <Link
          href="#works"
          onClick={(e) => {
            let el = document.getElementById("works");
            e.preventDefault();
            el && el.scrollIntoView({ behavior: "smooth", block: "start" });
            window.history.pushState("works", "works", "/works");
          }}
        >
          {t("works")}
        </Link>
      </NavBarLink>
      <NavBarLink>
        <Link
          href="#contact"
          onClick={(e) => {
            let el = document.getElementById("contact");
            e.preventDefault();
            el && el.scrollIntoView({ behavior: "smooth", block: "start" });
            window.history.pushState("contact", "contact", "/contact");
          }}
        >
          Contact
        </Link>
      </NavBarLink>
    </StyledNavBar>
  );
};

export default NavBar;
