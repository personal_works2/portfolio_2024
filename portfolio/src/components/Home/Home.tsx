import { useTranslation } from "react-i18next";
import styled, { keyframes } from "styled-components";
import { Logo, Ornament } from "../../Icon/Icon";
import { COLORS } from "../../utils/palette";

const slideFromTop = keyframes`
  from {
    top: -100%;
    opacity: 0;
  }
  to {
    top: 0;
    opacity: 1;
  }
`;

const slideFromLeft = keyframes`
  from {
    left: -100%;
    opacity: 0;
  }
  to {
    left: 0;
    opacity: 1;
  }
`;

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
  margin-top: 7.5%;
  margin-bottom: 15%;
  animation: ${fadeIn} 2.5s forwards;
`;

const HeaderContent = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;

  @media (max-width: 768px) {
    align-items: center;
  }
`;

const TextPresentation = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: start;
  width: 100%;

  @media (max-width: 768px) {
    font-size: 7vw;
  }
`;

const PresentationLocation = styled.p`
  font-family: sans-serif;
  font-variant: small-caps;
  font-size: 3vw;

  @media (max-width: 768px) {
    font-size: 10vw;
  }
`;

const Title = styled.h1`
  font-size: 5vw;
  margin: 0;
  position: relative;
  top: -100%;
  animation: ${slideFromTop} 2.5s forwards;

  @media (max-width: 768px) {
    font-size: 12vw;
  }
`;

const Em = styled.em`
  color: var(--sky-100);
  font-size: 7vw;
  @media (max-width: 768px) {
    font-size: 22vw;
  }
`;

const PresentationContent = styled.div`
  width: 70%;
  font-family: "Courier New";
  position: relative;
  animation: ${slideFromLeft} 2.5s forwards;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

const LogoWrapper = styled.div`
  position: relative;
  right: 19%;
  margin-top: 15%;
  animation: ${fadeIn} 2.5s forwards;

  @media (max-width: 768px) {
    display: none;
  }
`;

const AnimatedOrnament = styled(Ornament)`
  animation: spin 40s linear infinite;

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  @media (max-width: 768px) {
    display: none;
  }
`;

const Home = () => {
  const { t } = useTranslation();

  return (
    <HeaderContainer>
      <HeaderContent>
        <TextPresentation>
          <Title>
            {t("title")}
            <Em>Robin</Em>
          </Title>
          <PresentationContent>
            <PresentationLocation>
              {t("description.location")}
            </PresentationLocation>
            <p>
              {t("description.work.part1")} <b>Alvie</b>,{" "}
              {t("description.work.part2")}
            </p>
          </PresentationContent>
        </TextPresentation>
        <AnimatedOrnament
          fill={COLORS.GRASS[50]}
          width={"35vw"}
          height={"35vw"}
        ></AnimatedOrnament>
        <LogoWrapper>
          <Logo fill={COLORS.GRASS[100]} width={"12vw"} height={"12vw"} />
        </LogoWrapper>
      </HeaderContent>
    </HeaderContainer>
  );
};

export default Home;
