import React from "react";
import styled from "styled-components";

interface PicturesMosaicProps {
  images?: string[];
}

const MosaicContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 60px);
  grid-auto-rows: 60px;
  gap: 5px;
`;

const MosaicImage = styled.img`
  width: 100%;
  height: 100%;
  transition: transform 0.3s ease-in-out;
  &:hover {
    transform: scale(1.5);
  }
`;

const PicturesMosaic = ({ images }: PicturesMosaicProps): JSX.Element => {
  return (
    <MosaicContainer>
      {/* Mapping through the array of images and rendering each one */}
      {images?.map((image, index) => (
        <MosaicImage key={index} src={image} alt={`Image ${index + 1}`} />
      ))}
    </MosaicContainer>
  );
};

export default PicturesMosaic;
