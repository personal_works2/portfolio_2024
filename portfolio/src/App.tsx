import styled from "styled-components";
import "./App.css";
import GlobalStyles from "./components/GlobalStyle/GlobalStyle";
import LanguageSwitcher from "./components/LanguageSwitcher/LanguageSwitcher";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import FormContact from "./components/Form/Form";
import Works from "./components/Works/Works";
import NavBar from "./components/NavBar/NavBar";
import Footer from "./components/Footer/Footer";
import SocialNetworks from "./components/SocialNetworks/SocialNetworks";

const AppContainer = styled.div`
  z-index: 1;
`;

const App = (): JSX.Element => {
  return (
    <>
      <GlobalStyles />
      <AppContainer className="App">
        <SocialNetworks />
        <NavBar />
        <LanguageSwitcher />
        <Home />
        <About />
        <Works />
        <FormContact />
        <Footer />
      </AppContainer>
    </>
  );
};

export default App;
